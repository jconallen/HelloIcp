# use a node base image
FROM node:7-onbuild

# set maintainer
LABEL maintainer "miiro@getintodevops.com"

RUN echo "jenkins ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers

# tell docker what port to expose
EXPOSE 8000
